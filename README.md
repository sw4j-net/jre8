# Introduction

This project creates a docker image with JRE 8 installed on Debian Bookworm.

The image can be used to run applications using java.

This repository is mirrored to https://gitlab.com/sw4j-net/jre8
